package main

import (
	"bufio"
	"fmt"
	"net"
)

func main() {
	c, err := net.Dial("tcp", ":5000")
	if err != nil {
		fmt.Println(err)
		return
	}

	message, _ := bufio.NewReader(c).ReadString('\n')
	fmt.Print("->: " + message)
}
