package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"
)

var mux sync.Mutex

func main() {
	args := os.Args
	if len(args) != 2 {
		fmt.Println("need a number")
		return
	}
	num, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	var waitGroup sync.WaitGroup
	var i int

	kv := make(map[int]int)
	kv[1] = 12

	for i = 0; i < num; i++ {
		waitGroup.Add(1)
		go func(v int) {               //FIXED (ISSUE #2)
			defer waitGroup.Done()
			mux.Lock()             //FIXED (ISSUE #1, #2)
			kv[v] = v              //FIXED (ISSUE #1, #2)
			mux.Unlock()           //FIXED (ISSUE #1, #2)
		}(i)                           //FIXED (ISSUE #2)
	}

	waitGroup.Wait()                       //FIXED (ISSUE #3)
	kv[2] = 10
	fmt.Printf("kv = %#v\n", kv)
}
