# race condition
### the problem...
```
.
└── raceCondition.go
```
```
 25         kv := make(map[int]int)
 26         kv[1] = 12
 27 
 28         for i = 0; i < num; i++ {
 29                 waitGroup.Add(1)
 30                 go func() {
 31                         defer waitGroup.Done()
 32                         kv[i] = i //ISSUE #1: 'i' value is determined outside the anonymous function 
 33                 }()               //ISSUE #2: no argument is passed 
 34         }
 35 
 36         kv[2] = 10                //ISSUE #3: concurrent write operation  
 37         waitGroup.Wait()
```
you can verify for race conditions using the "-race" flag:
```
go run -race raceCondition.go 30
```
the command output...
```
==================
WARNING: DATA RACE
Read at 0x00c0000ba020 by goroutine 7:
  main.main.func1()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0x8c

Previous write at 0x00c0000ba020 by main goroutine:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:28 +0x264

Goroutine 7 (running) created at:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x237
==================
==================
WARNING: DATA RACE
Write at 0x00c0000a4150 by goroutine 7:
  runtime.mapassign_fast64()
      /usr/local/go/src/runtime/map_fast64.go:92 +0x0
  main.main.func1()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0xb9

Previous write at 0x00c0000a4150 by goroutine 8:
  runtime.mapassign_fast64()
      /usr/local/go/src/runtime/map_fast64.go:92 +0x0
  main.main.func1()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0xb9

Goroutine 7 (running) created at:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x237

Goroutine 8 (finished) created at:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x237
==================
WARNING: DATA RACE
Write at 0x00c0000c2050 by goroutine 7:
  main.main.func1()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0xf4

Previous write at 0x00c0000c2050 by goroutine 8:
  main.main.func1()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0xf4

Goroutine 7 (running) created at:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x237

Goroutine 8 (finished) created at:
  main.main()
      /home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x237
==================
fatal error: concurrent map writes

goroutine 34 [running]:
runtime.throw(0x522ad9, 0x15)
	/usr/local/go/src/runtime/panic.go:1117 +0x72 fp=0xc0000cdef8 sp=0xc0000cdec8 pc=0x465192
runtime.mapassign_fast64(0x5049a0, 0xc0000a4150, 0x10, 0x0)
	/usr/local/go/src/runtime/map_fast64.go:101 +0x36b fp=0xc0000cdf38 sp=0xc0000cdef8 pc=0x4432eb
main.main.func1(0xc0000ba010, 0xc0000ba020, 0xc0000a4150)
	/home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:32 +0xba fp=0xc0000cdfc8 sp=0xc0000cdf38 pc=0x4f551a
runtime.goexit()
	/usr/local/go/src/runtime/asm_amd64.s:1371 +0x1 fp=0xc0000cdfd0 sp=0xc0000cdfc8 pc=0x498841
created by main.main
	/home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x238

goroutine 1 [runnable]:
sync.(*WaitGroup).Add(0xc0000ba010, 0x1)
	/usr/local/go/src/sync/waitgroup.go:53 +0x2eb
main.main()
	/home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:29 +0x1f9

goroutine 35 [runnable]:
main.main.func1(0xc0000ba010, 0xc0000ba020, 0xc0000a4150)
	/home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30
created by main.main
	/home/caf/projects/zarya1/concurrency-patterns/race-condition/raceCondition.go:30 +0x238
exit status 2
```
### fixed...
```
.
└── noRaceCondition.go
```
```
 27         kv := make(map[int]int)
 28         kv[1] = 12
 29 
 30         for i = 0; i < num; i++ {
 31                 waitGroup.Add(1)
 32                 go func(v int) {               //FIXED (ISSUE #2)
 33                         defer waitGroup.Done()
 34                         mux.Lock()             //FIXED (ISSUE #1, #2)
 35                         kv[v] = v              //FIXED (ISSUE #1, #2)
 36                         mux.Unlock()           //FIXED (ISSUE #1, #2)
 37                 }(i)                           //FIXED (ISSUE #2)
 38         }
 39 
 40         waitGroup.Wait()                       //FIXED (ISSUE #3)
 41         kv[2] = 10
```
run the fixed example:
```
 go run -race noRaceCondition.go 30
```
the output now...
```
v = map[int]int{0:0, 1:1, 2:10, 3:3, 4:4, 5:5, 6:6, 7:7, 8:8, 9:9, 10:10, 11:11, 12:12, 13:13, 14:14, 15:15, 16:16, 17:17, 18:18, 19:19, 20:20, 21:21, 22:22, 23:23, 24:24, 25:25, 26:26, 27:27, 28:28, 29:29}
```
