package main

import (
	"fmt"
	"os"
	"strconv"
	"sync"
)

func main() {
	args := os.Args
	if len(args) != 2 {
		fmt.Println("need a number")
		return
	}
	num, err := strconv.Atoi(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	var waitGroup sync.WaitGroup
	var i int

	kv := make(map[int]int)
	kv[1] = 12

	for i = 0; i < num; i++ {
		waitGroup.Add(1)
		go func() {
			defer waitGroup.Done()
			kv[i] = i //ISSUE #1: 'i' value is determined outside the anonymous function 
		}()               //ISSUE #2: no argument is passed 
	}

	kv[2] = 10                //ISSUE #3: concurrent write operation  
	waitGroup.Wait()
	fmt.Printf("kv = %#v\n", kv)
}
